<?php

    function foo() { return 1; }

    function templ_foot() {
        $query_result = $db->query("SELECT * FROM tag");
        if (!$query_result) {
            exit("Gagal mengambil data tag");
        }
        $tags = $query_result->fetch_all(MYSQLI_ASSOC);
        $result = <<<'EOD'
        <div class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                         Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript: void(0);">About</a>
                            <a href="javascript: void(0);">Support</a>
                            <a href="javascript: void(0);">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
EOD;
        echo $result;
    }

    function templ_nav_item($name, $path, $icon, $current_path) {
        $active = strpos($current_path, $path) ? 'active' : '';
        return "<li class='nav-item'>
            <a class='nav-link $active' href='$path'><i class='$icon'></i>$name</a></a>
        </li>";
    }

    function templ_left_nav($current_path) {
        $cabang = templ_nav_item('Kantor Cabang', '/admin/cabang/list.php', 'fa fa-fw fa-rocket', $current_path);
        $kas = templ_nav_item('Kantor Kas', '/admin/kas/list.php', 'fa fa-fw fa-rocket', $current_path);

        $result = <<<EOD
<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Dashboard <span class="badge badge-success">6</span></a>
                        <div id="submenu-1" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1-2" aria-controls="submenu-1-2">E-Commerce</a>
                                    <div id="submenu-1-2" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="index.html">E Commerce Dashboard</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="ecommerce-product.html">Product List</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="ecommerce-product-single.html">Product Single</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="ecommerce-product-checkout.html">Product Checkout</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard-finance.html">Finance</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard-sales.html">Sales</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1-1" aria-controls="submenu-1-1">Infulencer</a>
                                    <div id="submenu-1-1" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="dashboard-influencer.html">Influencer</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="influencer-finder.html">Influencer Finder</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="influencer-profile.html">Influencer Profile</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    $cabang
                    $kas
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/uraian/list.php"><i class="fa fa-fw fa-rocket"></i>Uraian</a></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/gallery/list.php"><i class="fa fa-fw fa-rocket"></i>Gallery</a></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/berita/list.php"><i class="fa fa-fw fa-rocket"></i>Berita</a></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
EOD;
        echo $result;
    }

?>
