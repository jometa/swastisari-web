<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/49.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Perkembangan Anggota Kopdit Swastisari</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="mag-breadcrumb py-5">
    </div>

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-details-area">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <h4 class="post-title text-center">Tabel Perkembangan Anggota Kopdit Swasti Sari 3 tahun terakhir</h4>
                            <!-- Post Meta -->
                            
                                   
                                    <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th scope="col">No</th>
                                          <th scope="col">Uraian</th>
                                          <th scope="col">Tahun 2017 (Orang)</th>
                                          <th scope="col">Tahun 2018 (Orang)</th>
                                          <th scope="col">Tahun 2019 (Orang)</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th>1</th>
                                          <td>Pria</td>
                                          <td>19.425</td>
                                          <td>20.000</td>
                                          <td>20.000</td>
                                          
                                        </tr>
                                        <tr>
                                          <th>2</th>
                                          <td>Wanita</td>
                                          <td>19.830</td>
                                          <td>21.876</td>
                                          <td>21.876</td>
                                          
                                        </tr>
                                        <tr>
                                          <th colspan="2" style="text-align: center;">JUMLAH</th>
                                          <th>2222</th>
                                          <th>22222</th>
                                          <th>22222</th>
                                          
                                        </tr>
                                      </tbody>
                                    </table>
                                    
                                </div>
                           
                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->

    
    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>