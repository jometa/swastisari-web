<?php
  require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

  $id = $_GET['id'];
  $query_result = $db->query("SELECT * FROM berita WHERE id = $id");
  if (!$query_result) {
    exit('Gagal menambil data berita');
  }

  $item = $query_result->fetch_assoc();
  $konten = $item['konten'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(<?= $item['foto'] ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2><?= $item['judul'] ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Feature</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Archive by Category “TRAVEL”</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="post-details-area">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-thumb mb-30">
                            <img src="<?= $item['foto'] ?>" alt="">
                        </div>
                        <div class="blog-content">
                            <div class="post-meta">
                                <a href="#">MAY 8, 2018</a>
                            </div>
                            <h4 class="post-title"><?= $item['judul'] ?></h4>

                            <div id="news-content">
                            </div>

                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                    <?php
                      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/right-side-overview.php');
                    ?>
                </div>

            </div>
        </div>
    </section>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
    <script src="/admin/assets/libs/js/QuillDeltaToHtmlConverter.bundle.js"></script>
    <script>
        var konten_raw = "<?= $db->real_escape_string($konten) ?>";
        konten_raw = JSON.parse(konten_raw);
        var cfg = {};
        var converter = new QuillDeltaToHtmlConverter(konten_raw.ops, cfg);
        var content = converter.convert();

        $('#news-content').html(content);
        
    </script>
</body>

</html>