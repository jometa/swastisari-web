<?php
  require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/41.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>BERITA</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
    </div>


    <!-- ##### Archive Post Area Start ##### -->
    <div class="archive-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <div class="archive-posts-area bg-white p-30 mb-30 box-shadow">

                        <?php
	                        $take = 5;
	                        if (isset($_GET['take'])) {
	                            $take = (int)$_GET['take'];
	                        }

                          $query_result = $db->query("SELECT * FROM berita b LIMIT $take");
                          if (!$query_result) {
                            exit('error load data');
                          }
                          $items = $query_result->fetch_all(MYSQLI_ASSOC);
                          foreach ($items as $item) { ?>

                            <div class="single-catagory-post d-flex flex-wrap">
                                <!-- Thumbnail -->
                                <div class="post-thumbnail bg-img" style="background-image: url(<?= $item['foto'] ?>);">

                                </div>

                                <!-- Post Contetnt -->
                                <div class="post-content">
                                    <a href="/berita-detail.php?id=<?= $item['id'] ?>" class="post-title"><?= $item['judul'] ?></a>
                                    <!-- Post Meta -->
                                    <div class="post-meta-2">
                                        <?php 
                                            $query_tag = $db->query("
                                                    SELECT t.id, t.nama 
                                                        FROM berita b 
                                                        LEFT JOIN berita_tag bt ON b.id = bt.id_berita 
                                                        LEFT JOIN tag t ON t.id = bt.id_tag
                                                        WHERE b.id = $item[id]");
                                            if (!$query_tag) {
                                                exit('Gagal mengambil data');
                                            }
                                            $tags = $query_tag->fetch_all(MYSQLI_ASSOC);
                                            foreach ($tags as $tag) { ?>
                                                <div>
                                                    <span class="badge badge-primary"><?= $tag['nama'] ?></span>
                                                </div>
                                            <?php }
                                        ?>
                                    </div>
                                    <p><?= $item['tanggal_buat'] ?></p>
                                </div>
                            </div>

                          <?php }
                        ?>

                        <a 
                            href="<?=$_SERVER['PHP_SELF']?>?take=<?= $take + 5 ?>"
                            class="btn btn-lg btn-info btn-block"
                        >
                            Next
                        </a>

                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                    <div class="sidebar-area bg-white mb-30 box-shadow">
                    <?php
                      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/right-side-overview.php');
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>