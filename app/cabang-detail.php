<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    $query_result = $db->query("SELECT * FROM kantor_cbg WHERE id = $_GET[id]");

    if (!$query_result) {
        exit('Gagal mengambil data cabang dan kas');
    }

    $item = $query_result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(<?= $item['picture'] ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>DETAIL CABANG <?= $item['nama'] ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
    </div>


    <!-- ##### Archive Post Area Start ##### -->
    
    <div class="archive-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <div class="archive-posts-area bg-white p-30 mb-30 box-shadow">
                        
                        <div class="card-body">
                            <img class="mt-15" src="<?= $item['picture'] ?>" alt="">
                        </div>

                        
                    </div>
                    <div class="card my-4">
                        
                        <div class="card-header bg-white">
                            <h3>KONTAK</h3>
                        </div>

                        <div class="card-body">
                            <div>Alamat</div>
                            <h4>
                                <?= $item['alamat'] ?>
                            </h4>

                            <div>No. Telpon</div>
                            <h4>
                                <?= $item['no_tlpn'] ?>
                            </h4>

                            <div>Email</div>
                            <h4>
                                <?= $item['email'] ?>
                            </h4>
                        </div>
                   
                    </div>

                    <?php
                        $query_result = $db->query("SELECT * FROM kantor_kas WHERE kantor_kas.id_kantor_cbg = $_GET[id]");

                        if (!$query_result) {
                            exit('Gagal mengambil data cabang dan kas');
                        }

                        $items = $query_result->fetch_all(MYSQLI_ASSOC);
                    ?>

                    <div class="card my-4">
                        <div class="card-header bg-white">
                            <h3>Daftar Kantor Kas</h3>
                        </div>

                        <div class="card-body">
                            <?php 
                                foreach ($items as $kas) { ?>
                                    <div class="single-catagory-post d-flex flex-wrap">
                                        <!-- Thumbnail -->
                                        <div class="post-thumbnail bg-img" style="background-image: url(<?= $kas['picture'] ?>);">
                                           
                                        </div>
                                        <div class="post-content">
                                            <h4>Kantor Kas <?= $kas['nama'] ?></h4>
                                            <!-- Post Meta -->
                                            <div class="post-meta-2">
                                                <div>Alamat: <?= $kas['alamat'] ?></div>
                                                
                                                <div>No. Telpon: <?= $kas['no_tlpn'] ?></div>
                                            </div>
                                        </div>
                                            
                                    </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                    <div class="sidebar-area bg-white mb-30 box-shadow">
                    <?php
                      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/right-side-overview.php');
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>