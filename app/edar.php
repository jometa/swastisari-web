<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/49.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Perkembangan Anggota Kopdit Swastisari</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="mag-breadcrumb py-5">
    </div>

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-details-area">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <h4 class="post-title text-center">Tabel Pinjaman BeredarKopdit Swasti Sari 3 tahun terakhir</h4>
                            <!-- Post Meta -->
                                    <?php
                                      $query_result = $db->query("SELECT * FROM uraian WHERE tipe = 'PINJAMAN'");
                                      if (!$query_result) {
                                          exit('Gagal mengambil data');
                                      }

                                      $items = $query_result->fetch_all(MYSQLI_ASSOC);
                                      $mapped = array_map(function ($it) {
                                        return json_decode($it['konten']);
                                      }, $items);
                                      $merged = array_merge(...$mapped);
                                      $mapped = array_map(function ($it) {
                                        return $it->tahun;
                                      }, $merged);
                                      $nmapped = count($mapped);
                                      $max_year = date('Y');
                                      if ($nmapped > 0) {
                                        $max_year = max(...$mapped);
                                      }

                                      function rupiah($angka){
                                        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
                                        return $hasil_rupiah;
                                      }
                                    ?>
                                   
                                    <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th scope="col">No</th>
                                          <th scope="col">Uraian</th>
                                          <th scope="col">Tahun <?= $max_year - 2 ?> (Rp)</th>
                                          <th scope="col">Tahun <?= $max_year - 1 ?> (Rp)</th>
                                          <th scope="col">Tahun <?= $max_year - 0 ?> (Rp)</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                          foreach ($items as $idx => $item) { 
                                            $detail = json_decode($item['konten']);
                                            $reversed_detail = array_reverse($detail);
                                            $n = count($reversed_detail);
                                            ?>
                                            <tr>
                                              <th><?= $idx + 1 ?></th>
                                              <td><?= $item['nama'] ?></td>

                                              <td>
                                                <?php 
                                                  if ($n > 0) {
                                                    echo rupiah($reversed_detail[0]->nominal);
                                                  }
                                                ?>
                                              </td>

                                              <td>
                                                <?php 
                                                  if ($n > 1) {
                                                    echo rupiah($reversed_detail[1]->nominal);
                                                  }
                                                ?>
                                              </td>

                                              <td>
                                                <?php 
                                                  if ($n > 2) {
                                                    echo rupiah($reversed_detail[2]->nominal);
                                                  }
                                                ?>
                                              </td>
                                              
                                            </tr>    
                                          <?php }
                                        ?>                                        
                                      </tbody>
                                    </table>
                                    
                                </div>
                           
                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->

    
    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>