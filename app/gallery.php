<?php
  require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <style>
        .photo-gallery .row-photos .col-sm-4 {
            margin-top: 12px;
            margin-bottom: 12px;
        }

        .lb-data .lb-caption {
          font-size: 24px;
        }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <div class="photo-gallery" id="vue-app">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center py-5">
              <h2>GALLERY</h2>
              <hr>
            </div>
          </div>

          <div class="row photos">
            <?php
            $query_result = $db->query("SELECT * FROM media");
            if (!$query_result) {
              exit('error load data');
            }
            $items = $query_result->fetch_all(MYSQLI_ASSOC);
            foreach ($items as $item) { ?>
                <div class="col-sm-4 my-4"> 
                  <div class="card">
                    <img src="<?= $item['url'] ?>" class="card-img-top" alt="..." style="height: 200px">
                    <div class="card-body">
                      <h5 class="card-title"><?= $item['judul'] ?></h5>
                      <a href="<?= $item['url'] ?>" data-lightbox="photos" data-title="<?= $item['deskripsi'] ?>">Lihat Foto</a>
                    </div>
                  </div>
                </div>               
            <?php } ?>
          </div>

        </div>
    </div>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/vue-image-lightbox.min.js"></script>
    <script>
      <?php
        $query_result = $db->query("SELECT * FROM media");
        if (!$query_result) {
          exit('error load data');
        }
        $items = $query_result->fetch_all(MYSQLI_ASSOC);
      ?>

      var app = new Vue({
        el: '#vue-app',
        data: {
          items: <?= json_encode($items) ?>
        }
      });
    </script>
</body>

</html>