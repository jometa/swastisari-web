<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->

    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
	
    <title>Kopdit Swasti Sari</title>

  </head>
  <body>
     	<!-- menu bar -->
        <nav class="navbar navbar-expand-lg navbar-dark py-3 bg-primary fixed-top">
          <div class="container">
            <a class="navbar-brand" href="#">KOPDIT SWASTI SARI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="index.html">BERANDA <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="FInti.html">TENTANG KAMI</a>
                </li>
                <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="tentang_kami.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    TENTANG KAMI
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="FVisi.html">VISI DAN MISI</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="FInti.html">NILAI-NILAI INTI</a>                
                  </div>
                </li>
                 --><li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    PRODUK LAYANAN
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">SYARAT KEANGGOTAAN</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">SYARAT PINJAMAN</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">SIMPANAN</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">PINJAMAN</a>
                    
                  </div>
                </li>
                <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    GALLERY
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">FOTO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">VIDEO</a>
                  </div>
                </li> -->
                 <li class="nav-item">
                  <a class="nav-link" href="gallery.html">GALLERY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="FCabang.html">KANTOR CABANG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="about.html">KONTAK KAMI</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- akhir menu -->
        <!-- awal slider -->
     <div class="bd-example">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active" data-interval = "10000">
              <img src="img/slide1.png" class="d-block w-100" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </div>
            </div>
            <div class="carousel-item" data-interval = "10000">
              <img src="img/slide2.png" class="d-block w-100" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h5>Second slide label</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
            <div class="carousel-item" data-interval = "10000">
              <img src="img/slide3.png" class="d-block w-100" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h5>Third slide label</h5>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <!-- akhir slider-->
      <!-- footer -->
      <footer>
          <div class="container text-center">
            <div class="col-sm-12">
              <p>&copy; copyright 2019 | Built by <a href="http://facebook.com/rowindjuli">Rowin Djuli</a></p>            
            </div>
          </div>
        </footer>  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
  </body>

</html>