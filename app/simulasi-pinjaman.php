<?php
  require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>

    <style>
        .jumbotron {
          color: white;
          background: linear-gradient(to bottom, rgba(23, 162, 184, 0.9), rgba(23, 162, 184, 0.9)), url(/mag/img/bg-img/40.jpg);
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          background-attachment: fixed;
          height: 100vh;
        }


        .perkembangan-jumbotron {
          color: white;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          height: 60vh;
        }

        .simulasi-jumbotron {
          background: white;
          background-position: center;
          background-attachment: fixed;
          background-repeat: no-repeat;
          background-size: cover;
        }

        .pinjaman-table {
        	font-family: courier;
        	font-size: 12px;
        	font-weight: lighter;
        	border: 1px solid gray;
        	border-style: dashed;
        	border-collapse: collapse;
        }

        .table.pinjaman-table td, .table.pinjaman-table th {
        	border-top: 1px dashed gray;
        }
        .table.pinjaman-table th {
        	border-bottom: 3px dashed gray;
        }

        .table.pinjaman-table thead th {
        	text-transform: uppercase;
        }

        .table.pinjaman-table td, .table.pinjaman-table th {
        	padding: 0.3rem;
        }
/*
        .table.pinjaman-table thead {
        	border-bottom: 3px solid #eee;
        }*/

        .table.pinjaman-table tr td:nth-child(odd) {
        	background: #2b399138;
        }

        .table.pinjaman-table tr th:nth-child(odd) {
        	background: #2b399138;
        }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/41.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>SIMULASI PINJAMAN</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="padding-top: 120px; padding-bottom: 120px;">
       <div class="simulasi-jumbotron jumbotron-fluid mb-50" style="padding-top: 50px; padding-bottom: 50px; padding-left: 10px; padding-right: 10px;">
                  <div class="blog-content">
                    <blockquote>
                        <h8 class="quote-text font-weight-bold">*note </h8>
                        <br>
                        <h6 class="quote-text">- simulasi pinjaman ini semata-mata untuk memberikan gambaran kepada anggota yang
                        ingin mengajukan pinjaman di kopdit swasti sari</h6>
                        <h6 class="quote-text">- Rincian angsuran dibulatkan ke atas</h6> 
                    </blockquote>
                  </div>
                <div class="row" id="main-form">
                <div class="col-md-4">

                    <h2 class="font-weight-bold text-center">Simulasi Pinjaman</h2>
                    <div class="card bg-white">
                        <div class="card-body">
                          
                            <form>
                                <div class="form-group">
                                    <label class="col-form-label text-dark text-left">Jumlah Pinjaman</label>
                                    <currency-input
                                        v-model="jmlh_pinjaman"
                                        class="form-control"
                                        :distraction-free="distractionFree"
                                        :currency="currency"
                                    >
                                    </currency-input>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label text-dark text-left">Kontrak Pinjaman (bulan)</label>
                                    <input type="number" min=3 max=120 class="form-control" v-model="kontrak_pinjaman">
                                </div>
                                <div class="form-group">
                                    <label for="input-select" class='text-dark'>Jenis Bunga</label>
                                    <select class="form-control" id="jb-select" v-model="jb">
                                        <option value="1.6">Menurun - 1.6%</option>
                                        <option value="1.8">Menurun - 1.8%</option>
                                        <option value="1">Flat - 1%</option>
                                        <option value="0.96">Flat - 0.96%</option>
                                        <option value="0.5">Flat - 0.5%</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-8 col-xs-12">
                    <h2 class="font-weight-bold text-center">Contoh Rincian Angsuran</h2>
                    <div class="table-responsive">
                    <table class="table pinjaman-table">
                      <thead>
                        <tr>
                          <th scope="col">Bulan</th>
                          <th scope="col">Pokok</th>
                          <th scope="col">Bunga</th>
                          <th scope="col">Sukarela + Wajib</th>
                          <th scope="col">Total</th>
                          <th scope="col">Sisa Saldo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(r, idx) in forecast" :key="`forecast-row-${idx}`">
                          <td class="text-center">{{ idx + 1 }}</td>
                          <td>{{ r.pokok }}</td>
                          <td>{{ r.bunga }}</td>
                          <td>{{ r.ss_sw }}</td>
                          <td>{{ r.total_angsuran }}</td>
                          <td>{{ r.sisa_saldo_pinjaman }}</td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- ##### Mag Posts Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="/js/vue.js"></script>
    <script src="/js/vue-currency-input.umd.js"></script>
    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function rp(x) {
            return 'RP ' + x.toFixed(3).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

       var app = new Vue({
            el: '#main-form',
            data: {
                jmlh_pinjaman: 500000,
                kontrak_pinjaman: 3,
                jb: "1.6",

                hideNegligibleDecimalDigits: false,
                hideCurrencySymbol: false,
                hideGroupingSymbol: false,
                currency: {
                    prefix: 'RP '
                }
            },
            computed: {
                distractionFree () {
                  return {
                    hideNegligibleDecimalDigits: this.hideNegligibleDecimalDigits,
                    hideCurrencySymbol: this.hideCurrencySymbol,
                    hideGroupingSymbol: this.hideGroupingSymbol
                  }
                },
                forecast() {
                    var jb = parseFloat(this.jb);
                    var saldo = this.jmlh_pinjaman;

                    var flat = false;
                    if (jb <= 1) {
                        flat = true;
                    }

                    var result = [];
                    for (var i = 0; i < this.kontrak_pinjaman; i++) {
                        var pokok = this.jmlh_pinjaman / this.kontrak_pinjaman;
                        var bunga = flat ? (this.jmlh_pinjaman * (jb / 100.0)): saldo * (jb / 100.0);
                        var ss_sw = 50000;
                        var x = pokok + bunga + ss_sw;
                        saldo -= pokok;
                        result.push({
                            pokok: rp(pokok),
                            bunga: rp(bunga),
                            ss_sw: rp(ss_sw),
                            total_angsuran: rp(x),
                            sisa_saldo_pinjaman: rp(saldo)
                        });
                    }
                    return result;
                }
            },
            mounted() {
                var raw_input = localStorage.getItem('sw.forecast_input');
                var data = JSON.parse(raw_input);
                console.log(data);
                console.log(this.jmlh_pinjaman);
                console.log(data.jmlh_pinjaman);
                var that = this;
                Vue.nextTick(function () {
                    that.jmlh_pinjaman = data.jmlh_pinjaman;
                    that.kontrak_pinjaman = data.kontrak_pinjaman;
                    that.jb = data.jb;
                })
            }
       });
    </script>

</body>

</html>