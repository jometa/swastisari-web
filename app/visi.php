<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/49.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>VISI, MISI DAN NILAI-NILAI INTI KOPDIT SWASTI SARI</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="mag-breadcrumb py-5">
    </div>

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-details-area">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <h4 class="post-title text-center">VISI & MISI KOPDIT SWASTI SARI</h4>
                            <!-- Post Meta -->
                             <blockquote>
                                <h6 class="quote-name">VISI : </h6>
                                <br>
                                <h6>Menjadi Koperasi Kredit Terdepan dan Pilihan Masyarakat Indonesia</h6>
                            </blockquote>
                            <blockquote>
                                <h6 class="quote-name">MISI : </h6>
                                <br>
                                <h6>Menyediakan Pelayanan Keuangan secara profesional dan berkelanjuan untuk meningkatkan
                             Kesejahteraan Anggota</h6>
                            </blockquote>
                            <h4 class="post-title text-center">NILAI - NILAI INTI KOPDIT SWASTI SARI</h4>
                            <blockquote>
                                
                                <h6 class="quote-text text-center font-weight-bold">SEJAHTERA</h6>
                                <h6>S = Service (KepuasanMU Kebanggaan Kami)</h6>
                                <h6>E = Evolve (SuksesMu Kebahagiaan Kami)</h6>
                                <h6>J = Justice (Berdiri Sama Tinggi, Duduk Sama Rendah)</h6>
                                <h6>A = Advance (Terdepan Dalam Kualitas dan Kecepatan)</h6>
                                <h6>H = Honesty (Melakukan apa yang di katakan, Mengatakan apa yang di lakukan)</h6>
                                <h6>T = Togetherness (Berat sama dipikul, ringan sama di jinjing)</h6>
                                <h6>E = Earmark (7S "Senyum, Salam, Sapa, Sopan, Santun, Segera, Sukses")</h6>
                                <h6>R = Respect (Saling menghormati sesama)</h6>
                                <h6>A = Accountable (Berani Bertanggung Jawab Atas Ucapan Dan Tindakan)</h6>
                            </blockquote>
                            <blockquote>
                                <h6 class="quote-name text-center">KOPDIT SWASTI SARI</h6>
                                <h6 class="quote-text text-center">"Terdepan dan Terpercaya"</h6>
                            </blockquote>
                           
                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->

    
    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>