-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2019 at 05:53 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swastisari_db_2`
--
USE swastisari_db;

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `konten` text NOT NULL,
  `judul` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `tanggal_buat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `konten`, `judul`, `foto`, `tanggal_buat`) VALUES
(10, '{\"ops\":[{\"insert\":\"\\tKoperasi Kredit (Kopdit) Swasti Sari kini memiliki 19 kantor cabang. 18 kantor cabang diantaranya berada di 18 kabupaten di NTT. Sedangkan satu kantor cabang lainnya berada di Denpasar, Provinsi Bali. Pembukaan kantor cabang di Bali menandai ekspansi pertama Kopdit Swasti Sari di luar Provinsi NTT.\\n\\tEkspansi tersebut berjalan mulus setelah Kementerian Koperasi dan UKM melalui Deputi kelembagaan menyetujui proses amandemen/perubahan anggaran dasar Kopdit Swasti Sari dari primer provinsi ke primer nasional.\\n\\tGeneral Manager (GM) Kopdit Swasti Sari, Yohanes Sason Helan mengatakan, di tahun 2018 Kopdit Swasti Sari sukses melakukan ekspansi di Manggarai Barat, Manggarai, Manggarai Timur, Ende, Alor, Sabu Raijua dan Denpasar.\\n“Kantor cabang Denpasar adalah kantor cabang kita yang ke-19,” sebut Sason Helan kepada Timor Express, Rabu (12/12).\\n\\tSetelah sukes membuka kantor cabang di Denpasar, Sason Helan mengaku, pihaknya kini fokus untuk menuntaskan ekspansi di seluruh NTT. Sebab Kopdit Swasti Sari belum masuk di enam kabupaten. Yakni, Nagekeo, Ngada, Sikka, Sumba Tengah, Alor, dan Sumba Barat.\\n“Tinggal enam kabupaten itu yang akan kita tuntaskan di tahun 2019,” ungkap Sason Helan.\\nKantor Cabang Denpasar Diresmikan\\n\\tKantor Kopdit Swasti Sari Cabang Denpasar-Bali Minggu (9/12), akhirnya diresmikan oleh Kepala Dinas Koperasi dan UKM, I Gede Indra Dewa Putra. Hadir pada kesempatan itu, Kadis Koperasi dan UKM, Kosmas D. Lana, Kepala Dinas Koperasi dan UKM Kota Denpasar, Made Erwin Suryadarma Sena, dan Bendahara Dekopinwil Bali, Wayan Suyana, Sekretaris Inkopdit Bali, FX. Joniono Raharjo. Hadir pula Ketua Pengurus Swasti Sari, Martinus Seran serta sejumlah pengurus dan pengawas.\\nDalam sambutannya, Kadis Koperasi dan UKM Provinsi Bali I Gede Indra Dewa Putra mengapresiasi dan menyambut baik kehadiran Kopdit Swasti Sari di Denpasar. Sebab kehadiran kopdit, sangat membantu permodalan bagi anggota. Terlebih bagi masyarakat yang bergerak di usaha produktif.\\n“Bagi kami, Kopdit Swasti Sari bukan menjadi pesaing bagi koperasi yang ada di Bali. Namun justru sebagai pembanding agar koperasi di sini lebih cepat berkembang,” ujarnya.\\n\\tIndra Dewa Putra mengaku mencermati neraca lima tahun terakhir (dari 2014-2018), dimana pertumbuhan Kopdit Swasti Sari dari semua komponen rata-rata mencapai lebih dari 22 persen per tahun. Dan atas dasar inilah Kementerian Koperasi dan UKM melalui Deputi Kelembagaan tidak segan-segan memberikan keputusan primer nasional. Sekaligus merekomondasikan izin untuk membuka kantor cabang Kopdit Swasti Sari di Denpasar.\\n\\t“Secara pribadi dan kelembagaan dalam hal ini dinas koperasi dan Pemprov Bali, kami menyampaikan profisiat dan sukses buat Kopdit Swasti Sari. Semoga terus bertumbuh dan berkembang bersama anggota,” sebut Indra Dewa Putra.\\n\\tSementara Kepala Dinas Koperasi dan UKM Provinsi NTT, Kosmas D. Lana dalam sambutan mengatakan, berdasarkan penilaian kesehatan dan audit akuntan publik, Kopdit Swasti Sari tergolong sangat sehat untuk lima tahun terakhir.\\n\\tKhusus mengenai peralihan status dari primer provinsi ke primer nasional, Kosmas menyebutkan, proses yang dilewati Kopdit Swasti Sari sangat luar biasa. Pasalnya, waktu yang dibutuhkan hanya satu bulan lebih. Padahal koperasi-koperasi lain di Indonesia membutuhkan waktu tiga sampai empat tahun. Proses ini diakui Kosmas sangat cepat karena semua persyaratan sudah dipenuhi oleh Kopdit Swasti Sari.\\n\\t“Kami bangga karena Kopdit Swasti Sari bisa membawa nama NTT di Provinsi Bali. Ini sangat postif karena lewat kantor cabang di sini, ke depannya kami bisa menjual pariwisata NTT,” kata mantan Penjabat Bupati Nagekeo itu.\\n\\tYang membanggakan, lanjut Kosmas, meski baru beroperasi selama dua bulan lebih, namun anggota Kopdit Swasti Sari Cabang Denpasar sudah mencapai 500 orang dengan total aset mencapai Rp 1 miliar lebih.\\n\\t“Menurut saya, tidak ada lembaga keuangan lainnya yang bisa seperti ini. Jadi ke depannya, kami yakin Kopdit Swasti Sari bisa membantu anggota dan masyarakat Denpasar,” ungkapnya.\\nUntuk diketahui, berdasarkan kajian produk simpanan dan pinjaman, Kopdit Swasti Sari sangat berpihak kepada anggota dan masyarakat. Sebab bunga pinjamannya lebih kecil dari harga pasar. Sedangkan bunga simpanannya juga lebih besar, tiga sampai lima digit di atas harga pasar. Selain itu, tidak beban administrasi tiap bulan. Asuransi daperma setiap bulan yang nilainya ratusan juta juga tidak dibebankan kepada anggota.\\n\\tBagi anggota yang meninggal dunia, kepada ahli waris diberikan dana kematian sekitar Rp 9 juta –Rp 12 juta. Simpanannya dikembalikan dua kali lipat dan saldo pinjaman diputihkan. \"},{\"attributes\":{\"bold\":true},\"insert\":\"(tom)\"},{\"insert\":\"\\n\\n\"}]}', 'PERESMIAN KANTOR CABANG BALI', '/uploads/5dbf9031bf2b1.jpg', '2019-11-04 10:42:57'),
(12, '{\"ops\":[{\"insert\":\"Sesuai Rencana Kerja (RK) dan Rencana Anggaran (RA) tahun buku 2019 KSP Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" meresmikan 33 Kantor Kas.\\nKantor Kas yang baru hadir di tahun ini diantaranya Kantor Kas Oenfui, Maulafa, Sikumana, Batulesa, Noelbaki, Buraen, Lelogama, Pantai Baru, Nembrala, Raijua, Waijewa, Lewa, Melolo, Jumbaran Nusa Dua Bali, Tabanan Bali,\\n\"},{\"attributes\":{\"align\":\"center\"},\"insert\":\"\\n\"},{\"insert\":\"Selain itu di Detusoko, Maubesi, Halilulik, Rinhat, Kolbano, Niki-Niki, Waiklibang, Oka, Goyang Barang, Kenota, Kolilanang, Tanah Merah, ile Ape, Lebatukan dan Maubesi, Witihama dan Tuak Daun Merah (TDM).\\nGM KSP Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Yohanes Sason Helan, kepada POS-KUPANG. COM, Jumat (30/8/2019), mengatakan dari 33 kantor kas tang sudah diresmikan sebanyak 25 kantor kas yaitu Maulafa, Batulesa, Noelbaki\\nSelain itu ada di Buraen, Lelogama, Pantai Baru, Nembrala, Wewewa, Lewa, Melolo, Jimbaran, Detusoko, Maubesi, Halilulik, Rinhat, Kolbano, Niki2, Waiklibang, Oyang Baran, Kenota, Kolilanang, Tanah Merah dan Ile Ape Lebatukan.\\n\\\"Masih 8 yang akan diresmikan di September dan Oktober mendatang. Seperti di Penfui, Tuak Daun Merah (TDM), Raijua, Tabanan, Witihama, Oka, Sikumana dan Kedang,\\\" ujarnya.\\nPuluhan Kantor Kas ini dibuka di tahun ini, kata Yohanes, bertujuan untuk mendekatkan pelayanan atau menjemput bola. Dengan kajian anggota bisa hemat uang transport dan waktu.\\n\\n\\\"Semua ekspansi kantor sudah disetujui anggota dan anggota yang mengusulkan dengan kajian yang sangat matang, karena semua berdampak pada biaya. Jadi tidak sembarangan kami membuka kantor baru. Kami berjalan diatas rell yang telah disepakati sebagai landasan payung hukum,\\\" ujarnya. (*)(Laporan Reporter POS-KUPANG. COM, Yeni Rachmawati)\\n\\n\"}]}', 'Tahun Ini KSP Kopdit Swasti Sari Punya 33 Kantor Kas Baru, Yohanes : Kami Berjalan diatas Rel.', '/uploads/5dbf925032ff2.jpg', '2019-11-04 10:52:00'),
(13, '{\"ops\":[{\"insert\":\"Wacana sekitar 9 tahun yang lalu dari KSP Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" untuk membuka \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart, akhirnya terpenuhi.\\nHari ini \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart yang berlokasi di Jalan Timor Raya Kelurahan Pasir Panjang resmi dibuka oeh Gubernur NTT dalam hal ini diwakili Staf Ahli Politik dan Pemerintahan Setda NTT, Semuel Pakereng.\\n\"},{\"attributes\":{\"align\":\"center\"},\"insert\":\"\\n\"},{\"insert\":\"General Manager KSP Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" sekaligus Ketua Panitia, Yohanes Sason Helan, mengatakan peluncuran hari ini adalah suatu momen yang ditunggu-tunggu dan diharapkan oleh pemilik KSP Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" untuk bisa memenuhi semua kebutuhan yang diakomodir dalam \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart.\\nMomen ini, kata Yohanes sudah ditunggu oleh 70 ribuan \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\". Pasar yang jelas dan tidak rekayasa karena pasarnya \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" sendiri, sehingga manajemen tidak usah takut dengan pesaing-lesaing.\\nIa mengatakan \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" hadir untuk menjawabi yang benar dalam pelayanan. \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart tidak mencari keuntungan tetapi membagi kebersamaan dan keadilan. Dimana pada akhir tahun keuntungan akan dikembalikan kepada \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" 50 persen.\\n\\\"Daripada \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" berbelanja di tempat lain tapi tidak memperoleh keuntungan. Lebih baik belanja di \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart,\\\" tuturnya.\\nIa menekankan untuk tidak boleh takut dan gemetar dengan yang lainnya, karena \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart punya basis benar dan jelas.\\n\\\"Mari kita imbau kepada \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" untuk memanfaatkan \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart ini. Ini adalah yang pertama. Bila berhasil dan sukses maka ke depan akan dibangun \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Mart, toko \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\",\\\" tuturnya.\\nJika anggota mau berbelanja apapun di Swasti Sari Mart ada 1.800-an item yang disediakan.\\n\\n\\\"Barang sekecil apapun sudah ada. Kopdit Swastu Sari sudah punya mart dan toko \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/anggota\"},\"insert\":\"anggota\"},{\"insert\":\" kopdit swasti sari. Mari perkuat semua lini dan saya sudah imbau semua kantor untuk helanja di sini,\\\" tutupnya. (Laporan Reporter POS-KUPANG. COM, Yeni Rachmawati)\\n\\n\\n\"}]}', 'Swasti Sari Mart Diresmikan, Yohanes : Momen yang Ditunggu Para Anggota', '/uploads/5dbf931a55957.jpg', '2019-11-04 10:55:22'),
(14, '{\"ops\":[{\"insert\":\"Koperasi Kredit (kopdit) \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" menyerahkan Dana Perlindungan Bersama (Daperma) kepada 35 ahli waris dari anggota koperasi tersebut.\\nSeperti disaksikan Pos Kupang, Senin (13/5/2019), Ruang pelayanan Kantor Cabang Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" Kupang dipenuhi dengan puluhan ahli waris dari anggota Kopdit yang telah meninggal dunia.\\nDaperma merupakan salah satu produk dari Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" yang sangat membantu keluarga duka.\\n\"},{\"attributes\":{\"align\":\"center\"},\"insert\":\"\\n\"},{\"insert\":\"General Manager Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Yohanes Sason Helan, pada pembagian \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/daperma\"},\"insert\":\"Daperma\"},{\"insert\":\" di Kantor Cabang Wali Kota, Senin (13/5/2019), mengungkapkan sekecil apapun Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" tetap merawat anggotanya sampai pada kematian hingga pemakaman.\\nMenurutnya, secara keseluruhan NTT sekitar 80-an ahli waris yang menerima dana \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/daperma\"},\"insert\":\"Daperma\"},{\"insert\":\" dengan total dana Rp 2 miliar. Sedangkan khusus di Kota KupangRp 1 miliar.\\nPara ahli waris menerima Daperma bervariasi sesuai dengan besarnya saham dikali dua. Misalnya saham anggota Rp 5 juta, maka ahli waris menerima Rp 10 juta. Jadi ada yang menerima hampir Rp 50 juta, Rp 27 juta, Rp 14 juta, Rp 8 juta dan lainnya.\\n\\nSelain mendapatkan \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/daperma\"},\"insert\":\"Daperma\"},{\"insert\":\", pinjaman anggota pun telah diputihkan. Ia menambahkan, keunggulan dari \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/daperma\"},\"insert\":\"Daperma\"},{\"insert\":\" tidak dibayarkan atau dibebankan oleh anggota\\nYohanes berpesan agar uang yang diberikan jangan dipakai untuk berjudi, miras dan lainnya. Tetapi dimanfaatkan sebaik-baiknya.\\nIa meminta agar para ahli waris bisa mengajak keluarga, tetangga, teman yang belum menjadi anggota untuk bisa bergabung.\\n\\\"Jangan sembarangan investasi-investasi di lembaga keuangan bodong. Kami sudah 32 tahun berdiri, berarti sudah matang. Sudah ada 65 ribu anggota, dengan aset mencapai Rp 1 triliun murni dari anggota. Jadi jangan masuk di Kopdit seperti IGD,\\\" terangnya.\\nHampir setiap bulan, lanjutnya, ada sekitar 2.500 anggota baru yang masuk, dengan pertumbuhan sekitar Rp 15 miliar hingga Rp 18 miliar.\\n\"},{\"attributes\":{\"bold\":true},\"insert\":\"Terima Kasih Kopdit \"},{\"attributes\":{\"bold\":true,\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\"\\nPara ahli waris terlihat tersenyum dibalik kesedihan yang dialami mereka. Meskipun uang yang diterima tak mampu menghilangkan duka, tapi setidaknya bisa membantu keluarga yang ditinggalkan.\\nNama Marsel Ukar dipanggil urutan kedua untuk maju menerima \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/daperma\"},\"insert\":\"Daperma\"},{\"insert\":\" dari GM Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Yohanes Sason Helan.\\nSebagai ahli waris dari istrinya yang telah meninggal dunia pada 14 September 2018, Marsel menerima Daperma Rp 47 juta lebih. Selain ahli waris, dirinya juga sebagai anggota Kopdit Swasti Sari sejak tahun 2008.\\n\\nMarsel mengaku senang karena hak yang diberikan sesuai dengan kewajiban sebagai anggota. \\\"Kita sudah memberikan sesuai kemampuan kita. Kita menyimpan saham sesuai kemampuan kita dan dari pihak koperasi memberikan lebih, maka menyenangkan. Terima kasih kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\",\\\" tuturnya. (Laporan Reporter POS-KUPANG.COM, Yeni Rachmawati)\\n\\n\"}]}', '35 Ahli Waris Terima Daperma dari Swasti Sari', '/uploads/5dbf944118711.jpg', '2019-11-04 11:00:17'),
(15, '{\"ops\":[{\"insert\":\"Menteri Koperasi dan UKM mengeluarkan keputusan RI Nomor 000334/PAD/Dep.1/XI/ 2018 tanggal 12 November 2018 bahwa Koperasi Kredit (Kopdit) \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" naik status dari Kopdit Primer Provinsi menjadi Kopdit Primer Nasional.\\nKepala Dinas Koperasi dan UKM Provinsi NTT, Kosmas Lana, saat menerima General Manager Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Yohanes Sason Helan, di ruang kerjanya, Kamis (15/11/2018) mengatakan, Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" merupakan kopdit ketiga di NTT yang menjadi kopdit primer nasional. Sebelumnya Kopdit Pintu Air dan TLM.\\n\"},{\"attributes\":{\"align\":\"center\"},\"insert\":\"\\n\"},{\"insert\":\"\\\"Dengan keputusan itu, ruang lingkup usaha Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" yang sebelumnya lintas kabupaten dan kota menjadi lintas provinsi,\\\" tambahnya.\\nKosmas menyatakan, dalam peralihan status itu tidak ada kendala karena manajemen Kopdit Swasti Sari memenuhi semua persyaratan. Salah satunya, perubahan Anggaran Dasar (AD).\\n\\nMengenai perkembangan Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Kosmas menjelaskan, dalam tiga tahun terakhir Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" sangat sehat.\\n\\\"Laporan keuangan, pengurus dan manajemennya. Saya berharap dalam waktu tidak lama segera launching kantor cabang di Denpasar, Bali yang menurut rencana dihadiri Menteri Koperasi dan UKM. Saya berharap Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\" bisa lebih berkembang di Bali,\\\" kata Kosmas.\\nSeluruh pengurus yang bertugas di Bali, lanjut Kosmas, harus menunjukkan identitas NTT untuk membantu promosi budaya NTT.\\n\\n\\\"Pakai motif tenunan NTT sehingga NTT bisa dikenal luas,\\\" pesan Kosmas.\\nGeneral Manager (GM) Kopdit \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/tag/swasti-sari\"},\"insert\":\"Swasti Sari\"},{\"insert\":\", Yohanes Sason Helan mengatakan, salah satu syarat untuk menjadi kopdit nasional adalah amandemen anggaran dasar.\\n\\n\\nArtikel ini telah tayang di \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/2018/11/16/hebat-koperasi-swasti-sari-naik-status-jadi-tingkat-nasional\"},\"insert\":\"pos-kupang.com\"},{\"insert\":\" dengan judul Hebat, Koperasi Swasti Sari Naik Status Jadi Tingkat Nasional, \"},{\"attributes\":{\"color\":\"#016fba\",\"link\":\"https://kupang.tribunnews.com/2018/11/16/hebat-koperasi-swasti-sari-naik-status-jadi-tingkat-nasional\"},\"insert\":\"https://kupang.tribunnews.com/2018/11/16/hebat-koperasi-swasti-sari-naik-status-jadi-tingkat-nasional\"},{\"insert\":\".\\nPenulis: Adiana Ahmad\\nEditor: Hermina Pello\\n\"}]}', 'Hebat, Swasti Sari Naik Status Jadi Tingkat Nasional', '/uploads/5dbf94e677daa.jpg', '2019-11-04 11:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `berita_tag`
--

CREATE TABLE `berita_tag` (
  `id` int(11) NOT NULL,
  `id_berita` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita_tag`
--

INSERT INTO `berita_tag` (`id`, `id_berita`, `id_tag`) VALUES
(20, 12, 2),
(21, 13, 2),
(22, 14, 2),
(23, 15, 2),
(24, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_uraian`
--

CREATE TABLE `detail_uraian` (
  `id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `id_uraian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kantor_cbg`
--

CREATE TABLE `kantor_cbg` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlpn` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `picture` varchar(300) DEFAULT NULL,
  `jmlh_anggota` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kantor_cbg`
--

INSERT INTO `kantor_cbg` (`id`, `nama`, `alamat`, `no_tlpn`, `email`, `picture`, `jmlh_anggota`) VALUES
(5, 'Kota Kupang', 'Jln. Perintis Kemerdekaan, Kelapa Lima, Kota Kupang, NTT', '082145595764', 'kopditswastisarikotakupang@yahoo.com', '/uploads/5dbf1bffc41c9.JPG', 38266),
(6, 'Adonara', 'Jln. Trans Waiwerang, Desa Waiburak, Dusun Wailingo, Kec. Adonara Timur, Kab. Flores Timur, NTT', '081338738868', 'swastisari_tpadonara@yahoo.co.id', '/uploads/5dbf1e0e7b9cc.JPG', 2547),
(7, 'Kefamenanu', 'Jln. Ahmad Yani, Kel. Kefa Selatan, Kec. Kefamenanu, Kab. TTU, NTT', '081282704897', 'kopdit_swastisaritpkefa@yahoo.com', '/uploads/5dbf1f4923282.JPG', 7268),
(8, 'Oesao', 'Jln. Timor Raya, KM. 29 Oesao', '081339616423', 'kopditswastisarioesao@yahoo.com', '/uploads/5dbf1fd811095.JPG', 11249),
(9, 'Kantor Pusat', 'Jln. Sumba No 3c, Fatubesi, Kota Kupang, NTT', '03808438877', 'kopdit_swastisari@yahoo.com', '/uploads/5dbf202d4d782.JPG', 0),
(10, 'Sumba Barat Daya', 'Jln. Radamata, Desa Radamata, Kec. Tambolaka Weetebula', '082144889719', 'kspswastisari_cabangsbd@yahoo.com', '/uploads/5dbf21bd3ce53.JPG', 1817),
(11, 'Malaka', 'Jln. Maromak Oan, Desa Laran Wehali, Malaka', '081339429479', 'kopdit_swastisaricbg_malaka@yahoo.com', '/uploads/5dbf2238c3230.JPG', 1192),
(12, 'Lembata', 'Jln. Trans Lembata-Lamahora, Kel. Lewoleba, Kec. Nubatukan, Kab Lembata - NTT', '081246116363', 'kopdit_swastisarilwe@yahoo.com', '/uploads/5dbf23af3a0eb.JPG', 871),
(13, 'Atambua', 'Jln. Trans Timor KM. 2, Kel. Lidak, Kec. Atambua Selatan, Kab. Belu - NTT', '03808438877', 'kopdit_swastisariatb@yahoo.com', '/uploads/5dbf255676c48.JPG', 1266),
(14, 'Larantuka', 'Jln. San Juan No. 212, Kel. Sarotari Tengah, Kec. Larantuka, Kab. Flores Timur - NTT', '081239978093', 'kopdit_swastisarilka@yahoo.com', '/uploads/5dbf26aeb16e7.JPG', 2116),
(15, 'Soe', 'Jln. Diponegoro No. 22, Kel. Kampung Baru, Kec. Soe , Kab. TTS - NTT', '085339418430', 'kopdit_swastisarisoe@yahoo.com', '/uploads/5dbf28df58259.JPG', 2412),
(16, 'Rote Ndao', 'dsjdhskjd', '0934923', 'rote@gmail.com', '/uploads/5dc3d6263b86d.jpg', 1212),
(17, 'Waingapu', 'djshjds', '02812943', 'sumtim@gmail.com', '/uploads/5dc3d6617a1a0.jpg', 1234),
(18, 'Sabu', 'jsdslnc', '364746723', 'sabu@gmail.com', '/uploads/5dc3d685123cc.jpg', 1234),
(19, 'Denpasar', 'hjsddsncdsj', '0812637123', 'denpasar@gmail.com', '/uploads/5dc3d6ac7b718.jpg', 1234),
(20, 'Ende', 'djshjs', '934057', 'ende@gmail.com', '/uploads/5dc3d6ca58167.jpg', 1234),
(21, 'Ruteng', 'dgssa', '834249', 'ruteng@gmail.com', '/uploads/5dc3d6eedec54.jpg', 1234),
(22, 'Borong', 'sfhdjfk', '9048963', 'borong@gmail.com', '/uploads/5dc3d70faa67e.jpg', 1235),
(23, 'Labuan Bajo', 'jlnjhdjfsd', '0987654', 'lbj@gmail.com', '/uploads/5dc3d734e6402.jpg', 1234),
(24, 'Bajawa', 'jlngdhsadsgd', '09876543', 'bajawa@gmail.com', '/uploads/5dc3d75e4381b.jpg', 1234),
(25, 'Mbai', 'dfghjkl', '09876543', 'mbai@gmail.com', '/uploads/5dc3d77d27670.jpg', 1234),
(26, 'Sumba Barat', 'dfjkl', '0987654', 'sumbar@gmail.com', '/uploads/5dc3d7994b3fd.jpg', 1234),
(27, 'Sumba Tengah', 'jln.shajsdsa', '9876543', 'sumteng@gmail.com', '/uploads/5dc3d7c0db4bb.jpg', 1234);

-- --------------------------------------------------------

--
-- Table structure for table `kantor_kas`
--

CREATE TABLE `kantor_kas` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `id_kantor_cbg` int(11) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `no_tlpn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kantor_kas`
--

INSERT INTO `kantor_kas` (`id`, `nama`, `alamat`, `id_kantor_cbg`, `picture`, `no_tlpn`) VALUES
(2, 'Oeba', 'Jln. Sumba No 3c, Fatubesi, Kota Kupang, NTT', 5, '/uploads/5dbf2e9f70272.JPG', '082145595764'),
(3, 'Naikoten', 'Naikoten I, Kec. Kota Raja, Kota Kupang, NTT', 5, '/uploads/5dbf2f3ceab91.JPG', '03808438877'),
(4, 'Oesapa', 'Jln. Timor Raya KM. 8, Kel. Oesapa, Kec. Kelapa Lima, Kota Kupang - NTT', 5, '/uploads/5dbf2f860343c.JPG', '03808438877'),
(5, 'Alak', 'Jln. Yos Sudarso, Alak, Kota Kupang - NTT', 5, '/uploads/5dbf30bd56421.JPG', '03808438877'),
(6, 'Manulai', 'Jln. Jambu, Kec. Manulai II, Kota Kupang - NTT', 5, '/uploads/5dbf3109ea6b9.JPG', '03808438877'),
(7, 'Maulafa', 'Jln. Fetor Funay, Maulafa, Kota Kupang - NTT', 5, '/uploads/5dbf32445d6bc.JPG', '03808438877'),
(8, 'Semau', 'Pulau Semau', 5, '/uploads/5dbf3263715fd.JPG', '03808438877'),
(9, 'Baun', 'Jln. H. R. Koroh', 5, '/uploads/5dbf32a5b4a73.JPG', '03808438877'),
(10, 'Oepoi', 'Jln. Thamrin, Kota Kupang', 5, '/uploads/5dbf332997bfd.JPG', '03808438877'),
(11, 'Waiwadan', 'Jln. Trans Waiwadan', 6, '/uploads/5dbf337ed758e.JPG', '081338738868'),
(12, 'Tanah Merah', 'Jln. Trans Tanah Merah - Waiwadan', 6, '/uploads/5dbf33a7474f9.JPG', '081338738868'),
(13, 'Koli Lanang', 'Jln. Trans Koli Lanang-Sagu', 6, '/uploads/5dbf33e49c2fd.JPG', '081338738868'),
(14, 'Kenotan', 'Jln. Trans Kenotan, Adonara Tengah', 6, '/uploads/5dbf34145593c.jpg', '081338738868'),
(15, 'Oyang Barang', 'Jln. Trans Wotan Ulumado-Tobilota', 6, '/uploads/5dbf3441f399c.jpg', '081338738868'),
(16, 'Noemuti', 'Desa Noemuti, Kefamenanu - NTT', 7, '/uploads/5dbf34880e258.jpg', '081282704897'),
(17, 'Eban', 'Jln. Trans Eban - Aplal', 7, '/uploads/5dbf34bac089f.jpg', '081282704897'),
(18, 'Wini', 'Jln. Trans Perbatasan Wini ', 7, '/uploads/5dbf34e526119.jpg', '081282704897'),
(19, 'Insana', 'Jln. Timor Raya, Kel. Insana', 7, '/uploads/5dbf3524305a2.JPG', '081282704897'),
(20, 'Maubesi', 'Kel. Maubesi', 7, '/uploads/5dbf354e1128b.JPG', '081282704897'),
(21, 'Noelbaki', 'Jln. Timor Raya KM 15, Tanah Merah, Kab. Kupang - NTT', 8, '/uploads/5dbf35c330588.CR2', '081339616423');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `judul` varchar(255) NOT NULL,
  `id` int(11) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`judul`, `id`, `deskripsi`, `url`) VALUES
('PERESMIAN KANTOR', 5, 'Peresmian Kantor Cabang Sabu Raijua, Penyambutan Rombongan General Manager dan Pejabat pemerintah  dengan tarian tradisional sabu raijua', '/uploads/5dbf36c2952e6.jpg'),
('PERESMIAN KANTOR', 6, 'Peresmian Kantor Kas Boru cabang larantuka oleh kepala dinas kopersi dan Ukm flores timur', '/uploads/5dbf374342592.jpg'),
('PERESMIAN KANTOR', 7, 'Peresmian Kantor Kas Kapan, Cabang Soe. Foto bersama dengan para anggota yang bertempat tinggal di wilayah kapan dan sekitarnya. (RJD)', '/uploads/5dbf378e10385.jpg'),
('Pengobatan Gratis', 8, 'Pengobatan gratis di sabu raijua, kegiatan sosialisi ini biasa di lakukan saat membuka kantor cabang baru atau kantor kas baru, yang di dukung langsung oleh pemerintah dalam hal ini dinas kesehatan provinsi NTT, dan dinas kesehatan di kabupaten sabu raijua. (RJD)', '/uploads/5dbf381c2af26.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `nama`) VALUES
(1, 'bad'),
(2, 'good'),
(3, 'sport');

-- --------------------------------------------------------

--
-- Table structure for table `uraian`
--

CREATE TABLE `uraian` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tipe` enum('MODAL','PINJAMAN') NOT NULL,
  `konten` text NOT NULL DEFAULT '[]'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `uraian`
--

INSERT INTO `uraian` (`id`, `nama`, `tipe`, `konten`) VALUES
(1, 'SImpanan Pokok', 'MODAL', '[]'),
(2, 'ABC', 'MODAL', '[{\"tahun\":2017,\"nominal\":0}]'),
(3, 'ApOo', 'MODAL', '[{\"tahun\":2017,\"nominal\":34590000},{\"tahun\":2018,\"nominal\":54000000}]'),
(4, 'Simpanan Pokok', 'PINJAMAN', '[{\"tahun\":2017,\"nominal\":3176700000},{\"tahun\":2018,\"nominal\":3925500000}]'),
(5, 'Simpanan Anggota', 'PINJAMAN', '[{\"tahun\":2017,\"nominal\":3925500000},{\"tahun\":2018,\"nominal\":3176700000}]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita_tag`
--
ALTER TABLE `berita_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_berita` (`id_berita`),
  ADD KEY `id_tag` (`id_tag`);

--
-- Indexes for table `detail_uraian`
--
ALTER TABLE `detail_uraian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_uraian` (`id_uraian`);

--
-- Indexes for table `kantor_cbg`
--
ALTER TABLE `kantor_cbg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kantor_kas`
--
ALTER TABLE `kantor_kas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kantor_cbg` (`id_kantor_cbg`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`);

--
-- Indexes for table `uraian`
--
ALTER TABLE `uraian`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `berita_tag`
--
ALTER TABLE `berita_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `detail_uraian`
--
ALTER TABLE `detail_uraian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kantor_cbg`
--
ALTER TABLE `kantor_cbg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kantor_kas`
--
ALTER TABLE `kantor_kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `uraian`
--
ALTER TABLE `uraian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `berita_tag`
--
ALTER TABLE `berita_tag`
  ADD CONSTRAINT `fk_berita_tag_berita` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_berita_tag_tag` FOREIGN KEY (`id_berita`) REFERENCES `berita` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `detail_uraian`
--
ALTER TABLE `detail_uraian`
  ADD CONSTRAINT `fk_detail_uraian` FOREIGN KEY (`id_uraian`) REFERENCES `uraian` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kantor_kas`
--
ALTER TABLE `kantor_kas`
  ADD CONSTRAINT `fk_id_cabang` FOREIGN KEY (`id_kantor_cbg`) REFERENCES `kantor_cbg` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
